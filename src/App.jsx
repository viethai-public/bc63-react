import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { DemoFunctionComponent } from './DemoComponent/DemoFunctionComponent'
import DemoClassComponent from './DemoComponent/DemoClassComponent'
import { HomeComponent } from './BTComponent/HomeComponent'
import { BindingData } from './BindingData/BindingData'
import { RenderWithCondition } from './RenderWithCondition/RenderWithCondition'
import { HandleEvent } from './HandleEvent/HandleEvent'
import { RenderWithMap } from './RenderWithMap/RenderWithMap'
import { StyleComponent } from './StyleComponent/StyleComponent'
import { BTMapMovie } from './BTMapMovie/BTMapMovie'
import { DemoState } from './DemoState/DemoState'
import { DemoProps } from './DemoProps/DemoProps'
import { BTShoeShop } from './BTShoeShop/BTShoeShop'
import { DemoRedux } from './DemoRedux/DemoRedux'
import { BTPhoneRedux } from './BTPhoneRedux/BTPhoneRedux'
import { BTXucXac } from './BTXucXac/BTXucXac'
import { Routes, Route } from 'react-router-dom'
import { NotFound } from './components/NotFound'
import { PATH } from './constants/config'
import { MainLayout } from './layouts/MainLayout'
import { MovieDetail } from './BTMapMovie/MovieDetail'
import { BTForm } from './BTForm/BTForm'
import { DemoUseEffect } from './DemoUseEffect/DemoUseEffect'
import { DemoHooksReact } from './DemoHooksReact/DemoHooksReact'

// 2 loại component
// class component (stateful): life cycle

// function component (stateless): hooks
// hooks: về bản chất 1 function - đặt tên: use_

// JSX: javascript XML => cho phép viết cú pháp HTML ở trong javascript
// atribute của thẻ sẽ đc viết theo quy tắc camelCase
// class => className

// 1 component chỉ đc phép return về 1 thẻ JSX duy nhất

function App() {
    const [count, setCount] = useState(0)

    return (
        <div>
            <Routes>
                <Route element={<MainLayout />}>
                    {/* Trang chủ thêm thuộc tính index và bỏ thuộc tính path */}
                    <Route index element={<HomeComponent />} />

                    <Route path={PATH.bindingdata} element={<BindingData />} />
                    <Route path={PATH.renderwithcondition} element={<RenderWithCondition />} />
                    <Route path={PATH.handlevent} element={<HandleEvent />} />
                    <Route path={PATH.renderwithmap} element={<RenderWithMap />} />
                    <Route path={PATH.stylecomponent} element={<StyleComponent />} />

                    <Route path={PATH.btmovie} element={<BTMapMovie />} />

                    <Route path={PATH.movieDetail} element={<MovieDetail />} />

                    <Route path={PATH.demostate} element={<DemoState />} />
                    <Route path={PATH.demoprops} element={<DemoProps />} />
                    <Route path={PATH.btshoeshop} element={<BTShoeShop />} />
                    {/* <Route path={PATH.demoredux} element={<DemoRedux />} />
                <Route path={PATH.btphone} element={<BTPhoneRedux />} />
                <Route path={PATH.btxucxac} element={<BTXucXac />} /> */}

                    <Route path={PATH.redux.home}>
                        <Route index element={<DemoRedux />} />
                        <Route path={PATH.redux.btphone} element={<BTPhoneRedux />} />
                        <Route path={PATH.redux.btxucxac} element={<BTXucXac />} />
                    </Route>

                    {/* Khi user truy cập tới 1 đường dẫn ko tồn tại */}

                    <Route path={PATH.btForm} element={<BTForm />} />
                    <Route path={PATH.demoUseEffect} element={<DemoUseEffect />} />
                    <Route path={PATH.demoSomeHooks} element={<DemoHooksReact />} />
                </Route>

                <Route path="*" element={<NotFound />} />
            </Routes>
            {/* <DemoFunctionComponent />

            <DemoFunctionComponent></DemoFunctionComponent>

            <DemoClassComponent></DemoClassComponent> */}

            {/* Bài tập component */}
            {/* <HomeComponent /> */}

            {/* <BindingData /> */}

            {/* <RenderWithCondition /> */}

            {/* <HandleEvent /> */}

            {/* <RenderWithMap /> */}

            {/* <StyleComponent /> */}

            {/* <BTMapMovie /> */}

            {/* <DemoState />

            <DemoProps /> */}
            {/* 
            <BTShoeShop />

            <DemoRedux /> */}

            {/* <BTPhoneRedux />

            <BTXucXac /> */}
        </div>
    )
}

export default App
